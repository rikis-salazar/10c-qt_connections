#include "widget.h"
#include <QApplication>
#include <QProgressBar>
#include <QSlider>

// Connectting things is fun. Let us keep
// on doing this.

// How about creating our own signals and slots?
// Of course this is possible. The first step is
// to add (uncomment) the Q_OBJECT macro (see widget.h)
// Next we add a [private] slot to the class (widget.h),
// then we implement the slot (widget.cpp). Finally we
// connect the signal emitted by the window with
// our custom slot.

// Creating our own signals is even easier. Here, we will
// use a counter to close the program when the button is
// pressed 10 times.


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    /**
            //Let us keep this just in case
            // Create a different container window
            // Our old one is commented out
            QWidget window;
            window.setFixedSize(200, 80);

            // Create a progress bar with range [0,100],
            // and a starting value of 0
            QProgressBar *progressBar = new QProgressBar(&window);
            progressBar->setRange(0, 100);
            progressBar->setValue(0);
            progressBar->setGeometry(10, 10, 180, 30);

            // Create a horizontal slider with range [0,100],
            // and a starting value of 0
            QSlider *slider = new QSlider(&window);
            slider->setOrientation(Qt::Horizontal);
            slider->setRange(0, 100);
            slider->setValue(0);
            slider->setGeometry(10, 40, 180, 30);

            window.show();

            // Connection (no longer inside a class). This time
            // We'll use QObject::conect()
            QObject::connect(slider, SIGNAL( valueChanged(int) ),
                             progressBar, SLOT( setValue(int) ) );

    */

    Widget w;
    w.show();

    return a.exec();
}
