#include "widget.h"
#include "ui_widget.h"

#include <QPushButton>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    // Set fixed size for this window
    setFixedSize(80,40);

    // Create and position member button
    m_button = new QPushButton("Hola, mundo!", this);
    m_button->setGeometry(5,5,75,35);


    // Set the button to clickable and connect to this class
    m_button->setCheckable(true);

    m_counter = 0;

    connect( m_button, SIGNAL( clicked(bool) ),
             this, SLOT( slotButtonClicked(bool) ) );

    connect( this, SIGNAL( counterReached() ) ,
             QApplication::instance(), SLOT( quit() ) );

    /**
        We no longer need this. We made the point already

    // Perfom the connection via the
    // QApplication::instance() method
    connect(m_button, SIGNAL( clicked() ),
            QApplication::instance(), SLOT( quit() ) );

    */

}


void Widget::slotButtonClicked(bool checked){
    if (checked) {
        m_button->setText("Presionado.");
    }
    else{
        m_button->setText("Hola, mundo!");
    }

    ++m_counter;

    if ( m_counter == 10 ){
        emit counterReached();
    }
}

Widget::~Widget()
{
    delete ui;
}
