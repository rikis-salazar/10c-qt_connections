#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

namespace Ui {
class Widget;
}

// Forward declaration of object that will be used
// in Widget class below
class QPushButton;

class Widget : public QWidget
{
    Q_OBJECT   // Needed again.

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

signals:
    void counterReached();
    // ^^^ There is no need to implement this.
    //     The Q_OBJECT macro will do it for us,
    //     we can now emit the signal when needed.

// Since we are not going to make this available
// to the world, we'll keep it private.
private slots:
    void slotButtonClicked( bool checked );

private:
    Ui::Widget *ui;
    QPushButton *m_button;
    int m_counter;
};

#endif // WIDGET_H
